namespace StoreList.DataAccess.Contexts
{
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class StoreContext : DbContext
    {
        public DbSet<Store> Stores { get; set; }
        public DbSet<Product> Products { get; set; }
        
        public StoreContext(DbContextOptions<StoreContext> options)
            : base(options)
        {
            Database.Migrate();
        }
    }
}