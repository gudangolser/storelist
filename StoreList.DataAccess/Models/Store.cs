using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

    namespace StoreList.DataAccess.Models
{
    using System;

    public class Store
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string WorkHours { get; set; }
        public virtual List<Product> Products { get; set; }
        public Store()
        {
            Products = new List<Product>();
        }
    }
}