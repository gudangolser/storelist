using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreList.DataAccess.Models
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public Guid StoreId { get; set; }
        public virtual Store Store { get; set; }
    }
}