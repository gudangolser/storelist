﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.Common.Utils
{
    public class NotFoundException : BusinessLogicException
    {
        public NotFoundException(string message)
            : base($"entity {message} not found", System.Net.HttpStatusCode.NotFound)
        {
        }
    }
}
