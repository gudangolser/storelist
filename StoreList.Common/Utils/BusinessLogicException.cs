﻿using System;
using System.Net;

namespace StoreList.Common.Utils
{
    public abstract class BusinessLogicException : ApplicationException
    {
        public HttpStatusCode StatusCode { get; set; }
        

        public BusinessLogicException(string message, HttpStatusCode status = HttpStatusCode.InternalServerError)
            : base(message)
        {
            StatusCode = status;
        }
    }
}
