﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace StoreList.Common.Utils
{
    public class ValidationException : BusinessLogicException
    {
        public ValidationException(string message) : base(message, HttpStatusCode.Conflict)
        {
        }
    }
}
