﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using StoreList.Common.Utils;
using StoreList.Web.Controllers.Results;

namespace StoreList.Web.Filters
{
    public class ExceptionLoggerFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception != null)
            {
                var response = context.HttpContext.Response;
                if (context.Exception is BusinessLogicException ex)
                {
                    response.StatusCode = (int)HttpStatusCode.OK;
                    context.Result = new JsonResult(new SendingResult(true, ex.Message, ex.StatusCode));
                    
                }
                else
                {
                    response.StatusCode = (int)HttpStatusCode.OK;
                    context.Result = new JsonResult(new SendingResult(true, "Something went wrong", HttpStatusCode.InternalServerError));
                } 
            }
            base.OnException(context);
        }
    }
}
