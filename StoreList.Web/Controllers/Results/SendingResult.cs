﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StoreList.Web.Controllers.Results
{
    public class SendingResult
    {
        public object Model { get; set; }
        public string Message { get; set; }
        public List<string> MessageDetails { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public bool HasError { get; set; }

        public SendingResult(object model, string message = null)
        {
            Model = model;
            Message = message;
            StatusCode = HttpStatusCode.OK;
        }

        public SendingResult(ModelStateDictionary modelState)
        {
            StatusCode = HttpStatusCode.BadRequest;
            HasError = !modelState.IsValid;
            Message = GetStateError(modelState);
        }

        public SendingResult(bool hasError, string errorMessage, HttpStatusCode status = HttpStatusCode.InternalServerError)
        {
            StatusCode = status;
            HasError = hasError;
            Message = errorMessage;
        }

        public SendingResult(bool hasError,
                          string errorMessage,
                          List<string> messageDetails,
                          HttpStatusCode status = HttpStatusCode.InternalServerError)
            : this(hasError, errorMessage, status)
        {
            MessageDetails = messageDetails;
        }

        public string GetStateError(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                var sb = new StringBuilder();

                var messages = modelState.Values.ToList();

                for (int i = 0; i < modelState.Count; i++)
                {
                    var error = messages[i].Errors.FirstOrDefault();
                    if (error != null)
                        sb.AppendLine(string.Format("{0}", error.ErrorMessage));
                }

                return sb.ToString();
            }

            return string.Empty;
        }
    }
}
