﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoreList.BusinessLogic.Models;
using StoreList.BusinessLogic.Services.Interfaces;
using StoreList.Web.Controllers.Results;
using StoreList.Web.Filters;

namespace StoreList.Web.Controllers
{
    [ExceptionLoggerFilter]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService productService;
        public ProductController(IProductService productService)
        {
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        [HttpGet("get-products/{id}", Name ="GetProducts")]
        public SendingResult GetProducts(Guid id)
        {
            return new SendingResult(productService.GetProducts(id));
        }
        [HttpGet("{id}")]
        public SendingResult GetProduct(Guid id)
        {

            return new SendingResult(productService.Get(id));
        }
        
        [HttpPost]
        public SendingResult Post([FromBody] ProductCreateDto product)
        {
            productService.Create(product);
            return new SendingResult(true);
        }
        
        [HttpPut("{id}")]
        public SendingResult Put(Guid id, [FromBody] ProductUpdateDto product)
        {
            productService.Update(product);
            return new SendingResult(true);
        }
        
        [HttpDelete("{id}")]
        public SendingResult Delete(Guid id)
        {
            productService.Delete(id);
            return new SendingResult(true);
        }
    }
}