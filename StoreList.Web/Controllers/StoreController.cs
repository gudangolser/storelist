﻿using System;
using Microsoft.AspNetCore.Mvc;
using StoreList.BusinessLogic.Models;
using StoreList.BusinessLogic.Services.Interfaces;
using StoreList.Web.Controllers.Results;
using StoreList.Web.Filters;

namespace StoreList.Web.Controllers
{
    [ExceptionLoggerFilter]
    [Route("api/[controller]")]
    public class StoreController : Controller
    {
        private readonly IStoreService storeService;
        public StoreController(IStoreService storeService)
        {
            this.storeService = storeService ?? throw new ArgumentNullException(nameof(storeService));
        }
        
        [HttpGet]
        public SendingResult Get()
        {
            return new SendingResult(storeService.GetStores());
        }
        
        [HttpGet("{id}", Name = "Get")]
        public SendingResult Get(Guid id)
        {
            return new SendingResult(storeService.Get(id));
        }
        
        [HttpPost]
        public SendingResult Post([FromBody] StoreCreateDto store)
        {
            storeService.Create(store);
            return new SendingResult(true);
        }
        
        [HttpPut("{id}")]
        public SendingResult Put(Guid id, [FromBody] StoreUpdateDto store)
        {
            storeService.Update(store);
            return new SendingResult(true);
        }
        
        [HttpDelete("{id}")]
        public SendingResult Delete(Guid id)
        {
            storeService.Delete(id);
            return new SendingResult(true);
        }
    }
}
