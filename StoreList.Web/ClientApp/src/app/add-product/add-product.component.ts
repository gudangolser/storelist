import { Component, OnInit, Inject } from '@angular/core';
import { Product } from '../models/product.model';
import { HttpService } from '../services/http.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { delay } from "rxjs/operators"

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  public product: Product;
  public createMode: boolean;
  public showSpinner = false;

  constructor(private http: HttpService, public dialogRef: MatDialogRef<AddProductComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) { 
    if(data["Id"]) {
      this.createMode = false;
    } else {
      this.createMode = true;
      this.product = new Product();
      this.product.storeId = data["StoreId"];
    }
  }

  ngOnInit() {
    if(!this.createMode){
      this.http
      .get<Product>('api/Product/'+this.data["Id"])
      .subscribe(result => {
        this.product = result.model
      }, error => {
        this.dialogRef.close(false);
      });
    }
  }
  public saveChanges() {
    this.showSpinner = true;
    var id;
    if(this.createMode) {
      this.http
      .post<boolean>('api/Product', this.product)
      .pipe(
        delay(2000)
      )
      .subscribe(result => {
        if(result.hasError){
          this.showSpinner = false;
        } else {
          this.showSpinner = false;
          this.dialogRef.close(true);
        }
        }, error => {
            this.dialogRef.close(false);
          });
        }
    else {
      this.http
      .put<boolean>("api/Product/"+this.product.id,this.product)
      .pipe(
        delay(2000)
      )
      .subscribe(result => {
        if(result.hasError){
          this.showSpinner = false;
        } else {
          this.showSpinner = false;
          this.dialogRef.close(true);
        }
      }, error => {
        this.dialogRef.close(false);
      });
    }
  }

  onNoClick() {
    this.dialogRef.close(false);
  }

}
