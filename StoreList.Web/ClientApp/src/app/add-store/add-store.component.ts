import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '../models/store.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService } from '../services/http.service';
import { delay } from "rxjs/operators"

@Component({
  selector: 'app-add-store',
  templateUrl: './add-store.component.html',
  styleUrls: ['./add-store.component.css']
})
export class AddStoreComponent implements OnInit {
  public store: Store;
  public createMode: boolean;
  public showSpinner = false;

  constructor(private http: HttpService, public dialogRef: MatDialogRef<AddStoreComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) { 
    if(data["Id"]) {
      this.createMode = false;
    } else {
      this.createMode = true;
      this.store = new Store();
    }
  }

  ngOnInit() {
    if(!this.createMode){
      this.http
      .get<Store>('api/Store/'+this.data["Id"])
      .subscribe(result => {
        this.store = result.model
      }, error => {
        this.dialogRef.close(false);
      });
    }
  }

  public saveChanges() {
    this.showSpinner = true;
    var id;
    if(this.createMode) {
      this.http
      .post<boolean>('api/Store', this.store)
      .pipe(
        delay(2000)
      )
      .subscribe(result => {
          if(result.hasError){
            this.showSpinner = false;
          } else {
            this.showSpinner = false;
            this.dialogRef.close(true);
          }
        }, error => {
            this.showSpinner = false;
            this.dialogRef.close(false);
          });
        }
    else {
      this.http
      .put<boolean>("api/Store/"+this.store.id,this.store)
      .pipe(
        delay(2000)
      )
      .subscribe(result => {
        if(result.hasError){
          this.showSpinner = false;
        } else {
          this.showSpinner = false;
          this.dialogRef.close(true);
        }
      }, error => {
        this.showSpinner = false;
        this.dialogRef.close(false);
      });
    }
  }

  onNoClick() {
    this.dialogRef.close(false);
  }
}
