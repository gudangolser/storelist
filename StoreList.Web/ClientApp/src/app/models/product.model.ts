import { Guid } from "guid-typescript";

export class Product implements IProduct {
    id: Guid;
    name: string;
    comment: string;
    storeId: Guid;
}

export interface IProduct{
    id: Guid;
    name: string;
    comment: string;
    storeId: Guid;
}