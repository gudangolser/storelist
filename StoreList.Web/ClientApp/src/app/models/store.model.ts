import { Guid } from "guid-typescript";
export class Store implements IStore {
    id: Guid;
    name: string;
    address: string;
    workHours: string;
}

export interface IStore{
    id: Guid;
    name: string;
    address: string;
    workHours: string;
}