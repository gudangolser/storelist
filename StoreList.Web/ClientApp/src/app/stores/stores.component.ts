import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IStore } from '../models/store.model';
import { SendingResult } from '../models/sending-result.model';
import { MatDialog } from '@angular/material/dialog';
import { AddStoreComponent } from '../add-store/add-store.component';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent {

  public stores: IStore[];

  constructor(private http: HttpService, public dialog: MatDialog) {
    this.getStores();
  }

  getStores(){
    this.http.get<IStore[]>('api/Store').subscribe(result => {
      this.stores = result.model;
    }, error => console.error(error));
  }
  create(){
    let dialogRef = this.dialog.open(AddStoreComponent, {
      width: '400pt',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getStores();
      }
    });
  }
  edit(id){
    let dialogRef = this.dialog.open(AddStoreComponent, {
      width: '400pt',
      data: {Id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getStores();
      }
    });
  }
  delete(id){
    this.http.delete<boolean>('api/Store/'+id).subscribe(result => {
      this.getStores();
    }, error => console.error(error));
  }
}