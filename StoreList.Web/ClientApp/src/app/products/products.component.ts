import { Component, OnInit } from '@angular/core';
import { IProduct } from '../models/product.model';
import { HttpService } from '../services/http.service';
import { MatDialog } from '@angular/material/dialog';
import { AddProductComponent } from '../add-product/add-product.component';
import { ActivatedRoute } from '@angular/router';
import { Guid } from 'guid-typescript';
import { IStore } from '../models/store.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: IProduct[]; 
  public storeId: Guid;
  public store: IStore;

  constructor( private http: HttpService, public dialog: MatDialog, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.storeId = params['id'];
      if(this.storeId) {
        this.getProducts(this.storeId);
        this.getStore(this.storeId);
      }
    });
  }
  getProducts(id){
      this.http.get<IProduct[]>("api/Product/get-products/"+this.storeId)
        .subscribe(result=>{
            this.products = result.model;
        }, error => console.error(error));
  }
  getStore(id){
    this.http.get<IStore>("api/Store/"+this.storeId)
        .subscribe(result=>{
            this.store = result.model;
        }, error => console.error(error));
  }
  create(){
    let dialogRef = this.dialog.open(AddProductComponent, {
      width: '400pt',
      data: {StoreId: this.storeId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getProducts(this.storeId);
      }
    });
  }
  edit(id){
    let dialogRef = this.dialog.open(AddProductComponent, {
      width: '400pt',
      data: {Id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getProducts(this.storeId);
      }
    });
  }
  delete(id){
    this.http.delete<boolean>('api/Product/'+id).subscribe(result => {
      this.getProducts(this.storeId);
    }, error => console.error(error));
  }
}
