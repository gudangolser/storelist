import {Injectable, Inject} from '@angular/core';
import {Jsonp, RequestOptionsArgs} from '@angular/http';
import {HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';

import { NotifierService } from 'angular-notifier';

import {environment} from '../../environments/environment';
import { SendingResult } from '../models/sending-result.model';

@Injectable()
export class HttpService {
    //private  baseUrl = environment.baseUrl;
    private httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
    
    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private readonly notifier: NotifierService, private navigator: Router) {
    }

    get<T> (path: string, params?: string) : Observable<SendingResult<T>> {
       var stringForGet = '';
        if(params){
            stringForGet = params;
        }
        return this.http
        .get<SendingResult<T>>(this.baseUrl + path + stringForGet)
        .pipe(
            catchError(this.handleError),
            map(this.extractData.bind(this))
        );
        //.map(this.extractData.bind(this))
        //.catch(this.handleError);
    }

    post<T> (path: string, param: any, options?: RequestOptionsArgs): Observable<SendingResult<T>> {
        
        const body = JSON.stringify(param);
        return this.http
        .post<SendingResult<T>>(this.baseUrl + path, body, this.httpOptions)
        .pipe(
            catchError(this.handleError),
            map(this.extractData.bind(this)),
            map(this.notify.bind(this))
        );
        // .map(this.extractData.bind(this))
        // .map(this.notify.bind(this))
        // .catch(this.handleError);
    }

    put<T> (path: string, param: any, options?: RequestOptionsArgs): Observable<SendingResult<T>> {
        const body = JSON.stringify(param);

        return this.http
        .put<SendingResult<T>>(this.baseUrl + path, body, this.httpOptions)
        .pipe(
            catchError(this.handleError),
            map(this.extractData.bind(this)),
            map(this.notify.bind(this))
        );
        // .map(this.extractData.bind(this))
        // .map(this.notify.bind(this))
        // .catch(this.handleError);
    }
    delete<T> (path: string, params?: string) : Observable<SendingResult<T>> {

        return this.http
        .delete<SendingResult<T>>(this.baseUrl + path, this.httpOptions)
        .pipe(
            catchError(this.handleError),
            map(this.extractData.bind(this)),
            map(this.notify.bind(this))
        );
        // .map(this.extractData.bind(this))
        // .map(this.notify.bind(this))
        // .catch(this.handleError);
    }

    private extractData<T> (result: SendingResult<T>) {
        var message = "";
         if (result.hasError) {
            message = result.message;
            if(result.messageDetails && result.messageDetails.length>0) {
                message += ": "+result.messageDetails.join(", ");
            }
            this.notifier.notify( 'error', message );
        }
        return result;
    }

    private notify<T> (result: SendingResult<T>) {
        console.log("Hi!");
        console.log(result);
        if (result.model) {
           this.notifier.notify( 'success', 'success');
        }
       return result;
   }
    
    public handleError = (error: HttpErrorResponse) => {
        if(error.status==0) {
            this.navigator.navigate(['/error',error.status, "Server not available"]);
            return Observable.throw("Server not available");
        }
        else if(error.status==400){
            this.notifier.notify( 'error', error.error.message );
        }
        else {
            var message = (error.error  && error.error.message) ? error.error.message : "Error has occured";
            this.navigator.navigate(['/error',error.status, message]);
        }
        return Observable.throw(error);
    }
}