import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
    selector: 'confirm-window',
    templateUrl: './confirm-window.component.html'
  })
  export class WindowComponent implements OnInit {

    message: string;

    ngOnInit(): void {
    }

    constructor(public dialogRef: MatDialogRef<WindowComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
        this.message = data["message"];
    }
    onOk() {
        this.dialogRef.close(true);
    }

    onCancel() {
        this.dialogRef.close(false);
    }
}