import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule, MatButtonModule, MatInputModule, MatCardModule } from '@angular/material';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';


import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { StoresComponent } from './stores/stores.component';
import { AddStoreComponent } from './add-store/add-store.component';
import { WindowComponent } from './common/confirm-window/confirm-window.component';
import { HttpService } from './services/http.service';
import { NotifierService, NotifierModule } from 'angular-notifier';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductsComponent } from './products/products.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    StoresComponent,
    AddStoreComponent,
    WindowComponent,
    AddProductComponent,
    ProductsComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    NotifierModule,
    RouterModule.forRoot([
      { path: '', component: StoresComponent, pathMatch: 'full' },
      { path: 'stores', component: StoresComponent },
      { path: 'products/:id', component: ProductsComponent },
    ])
  ],
  providers: [
    HttpService,
    NotifierService,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS, 
      useValue: {hasBackdrop: true}
    }
  ],
  entryComponents: [
    WindowComponent,
    AddStoreComponent,
    AddProductComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
