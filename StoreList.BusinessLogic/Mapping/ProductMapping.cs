﻿using AutoMapper;
using StoreList.BusinessLogic.Models;
using StoreList.DataAccess.Models;

namespace StoreList.BusinessLogic.Mapping
{
    public class ProductMapping : Profile
    {
        public ProductMapping()
        {
            CreateMap<Product, ProductDto>(MemberList.None)
                    .ReverseMap();
            CreateMap<ProductCreateDto, Product>(MemberList.None);
            CreateMap<ProductUpdateDto, Product>(MemberList.None);
        }
    }
}
