﻿using AutoMapper;
using StoreList.BusinessLogic.Models;
using StoreList.DataAccess.Models;

namespace StoreList.BusinessLogic.Mapping
{
    public class StoreMapping : Profile
    {
        public StoreMapping()
        {
            CreateMap<Store, StoreDto>(MemberList.None)
                .ReverseMap();
            CreateMap<StoreCreateDto, Store>(MemberList.None);
            CreateMap<StoreUpdateDto, Store>(MemberList.None);
        }
    }
}
