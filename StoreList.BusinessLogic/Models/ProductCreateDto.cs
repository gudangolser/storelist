﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Models
{
    public class ProductCreateDto
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public Guid StoreId { get; set; }
    }
}
