﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Models
{
    public class ProductUpdateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}
