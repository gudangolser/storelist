﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Models
{
    public class StoreCreateDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string WorkHours { get; set; }
    }
}
