﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Models
{
    public class StoreUpdateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string WorkHours { get; set; }
    }
}
