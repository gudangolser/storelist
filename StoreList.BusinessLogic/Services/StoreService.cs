﻿using AutoMapper;
using StoreList.BusinessLogic.Models;
using StoreList.BusinessLogic.Properties;
using StoreList.BusinessLogic.Services.Interfaces;
using StoreList.Common.Utils;
using StoreList.DataAccess.Contexts;
using StoreList.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreList.BusinessLogic.Services
{
    class StoreService : IStoreService
    {
        private readonly StoreContext db;
        private readonly IMapper mapper;
        public StoreService(StoreContext dbContext, IMapper mapper)
        {
            db = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(StoreCreateDto storeCreate)
        {
            if(db.Stores.Any(x => x.Name == storeCreate.Name && x.Address == storeCreate.Address))
            {
                throw new ValidationException(Resources.Store_Already_Exist_Exception);
            }
            var store = mapper.Map<Store>(storeCreate);
            db.Stores.Add(store);
            db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var store = db.Stores.FirstOrDefault(x => x.Id == id);
            if (store != null)
            {
                db.Products.RemoveRange(store.Products);
                db.Stores.Remove(store);
                db.SaveChanges();
                return;
            }
            throw new NotFoundException(Resources.Store);
        }

        public StoreDto Get(Guid id)
        {
            var store = db.Stores.FirstOrDefault(x => x.Id == id);
            if(store != null)
            {
                return mapper.Map<StoreDto>(store);
            }
            throw new NotFoundException(Resources.Store);
        }

        public List<StoreDto> GetStores()
        {
            return mapper.ProjectTo<StoreDto>(db.Stores).ToList();
        }

        public void Update(StoreUpdateDto storeUpdate)
        {
            var store = db.Stores.FirstOrDefault(x => x.Id == storeUpdate.Id);
            if (store != null)
            {
                if (db.Stores.Any(x => x.Name == storeUpdate.Name && x.Address == storeUpdate.Address && x.Id != storeUpdate.Id))
                {
                    throw new ValidationException(Resources.Store_Already_Exist_Exception);
                }
                mapper.Map(storeUpdate, store);
                db.SaveChanges();
                return;
            }
            throw new NotFoundException(Resources.Store);
        }
    }
}
