﻿using StoreList.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Services.Interfaces
{
    public interface IStoreService
    {
        void Create(StoreCreateDto product);
        void Update(StoreUpdateDto product);
        void Delete(Guid id);
        StoreDto Get(Guid id);
        List<StoreDto> GetStores();
    }
}
