﻿using StoreList.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Services.Interfaces
{
    public interface IProductService
    {
        void Create(ProductCreateDto product);
        void Update(ProductUpdateDto product);
        void Delete(Guid id);
        ProductDto Get(Guid id);
        List<ProductDto> GetProducts(Guid storeId);
    }
}
