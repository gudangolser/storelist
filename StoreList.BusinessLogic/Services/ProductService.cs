﻿using AutoMapper;
using StoreList.BusinessLogic.Models;
using StoreList.BusinessLogic.Properties;
using StoreList.BusinessLogic.Services.Interfaces;
using StoreList.Common.Utils;
using StoreList.DataAccess.Contexts;
using StoreList.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreList.BusinessLogic.Services
{
    class ProductService : IProductService
    {
        private readonly StoreContext db;
        private readonly IMapper mapper;
        public ProductService(StoreContext dbContext, IMapper mapper)
        {
            db = dbContext ?? throw new NullReferenceException(nameof(dbContext));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(ProductCreateDto productCreate)
        {
            if (db.Products.Where(x => x.StoreId == productCreate.StoreId).Any(x => x.Name == productCreate.Name))
            {
                throw new ValidationException(Resources.Product_Already_Exist_Exception);
            }
            var product = mapper.Map<Product>(productCreate);
            db.Products.Add(product);
            db.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = db.Products.FirstOrDefault(x => x.Id==id);
            if (entity != null)
            {
                db.Products.Remove(entity);
                db.SaveChanges();
                return;
            }
            throw new NotFoundException(Resources.Product);
        }

        public ProductDto Get(Guid id)
        {
            var entity = db.Products.FirstOrDefault(x => x.Id == id);
            if (entity != null)
            {
                return  mapper.Map<ProductDto>(entity);
            }
            throw new NotFoundException(Resources.Product);
        }

        public List<ProductDto> GetProducts(Guid storeId)
        {
            return mapper.ProjectTo<ProductDto>(db.Products.Where(x => x.StoreId == storeId)).ToList();
        }

        public void Update(ProductUpdateDto productUpdate)
        {
            var product = db.Products.FirstOrDefault(x => x.Id == productUpdate.Id);
            if (product != null)
            {
                if (db.Products.Where(x => x.StoreId == product.StoreId && x.Id != product.Id).Any(x => x.Name == product.Name))
                {
                    throw new ValidationException(Resources.Product_Already_Exist_Exception);
                }
                mapper.Map(productUpdate, product);
                db.SaveChanges();
                return;
            }
            throw new NotFoundException(Resources.Product);
        }
    }
}
