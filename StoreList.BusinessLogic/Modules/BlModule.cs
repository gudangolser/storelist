﻿using Autofac;
using StoreList.BusinessLogic.Services;
using StoreList.BusinessLogic.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreList.BusinessLogic.Modules
{
    public class BlModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StoreService>().As<IStoreService>();
            builder.RegisterType<ProductService>().As<IProductService>();

        }
    }
}
